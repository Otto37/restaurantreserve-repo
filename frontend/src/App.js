import "./App.css";
import { React, useEffect, useState } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import Login from "./components/login";
import AddReview from "./components/add-review";
import RestaurantsList from "./components/restaurants-list";
import Restaurants from "./components/restaurants";

function SetTITLE() {
  useEffect(() => {
    document.title = "Bruh";
  });
}

function App() {
  const [user, setUser] = useState(null);
  useEffect(() => {
    document.title = "BRueh";
  }, []);
  //temporary login logout system

  async function login(user = null) {
    setUser(user);
  }
  async function logout() {
    setUser(null);
  }
  return (
    <div className="App">
      <nav
        class="navbar navbar-expand-lg navbar-dark bg-secondary"
        id="navbarWrapper"
      >
        <a class="navbar-brand" href="#">
          <img
            src="booking-icon-23.png"
            alt="Booking-logo"
            id="navbarLogo"
          ></img>
        </a>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
          id="navbarToggler"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link text-white" href="#">
                Home
              </a>
            </li>
            <li class="nav-item" id="navbarLogin">
              {user ? (
                <a
                  onClick={logout}
                  className="nav-link"
                  style={{ cursor: "pointer" }}
                >
                  Logout {user.name}
                </a>
              ) : (
                <Link to={"/login"} className="Nav-link">
                  <a class="nav-link text-white" href="#">
                    Login
                  </a>
                </Link>
              )}
            </li>
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle text-white"
                href="#"
                id="navbarDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Dropdown
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">
                  Action
                </a>
                <a class="dropdown-item" href="#">
                  Another action
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">
                  Something else here
                </a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="#">
                Disabled
              </a>
            </li>
          </ul>

          <form class="form-inline my-2 my-lg-0 text-white" id="navbarSearch">
            <input
              class="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            ></input>
            <button
              class="btn btn-outline-warning my-2 my-sm-0 text-weight-bold"
              type="submit"
              id="navbarSubmit"
            >
              Search{"  "}
              <i class="fas fa-search" style={{ "margin-left": "5px" }}></i>
            </button>
          </form>
        </div>
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route
            exact
            path={["/", "/restaurants"]}
            component={RestaurantsList}
          />
          <Route
            path="/restaurants/:id/review"
            render={(props) => <AddReview {...props} user={user} />}
          />
          <Route
            path="/restaurants/:id"
            render={(props) => <Restaurants {...props} user={user} />}
          />
          <Route
            path="/login"
            render={(props) => <Login {...props} login={login} />}
          />
        </Switch>
      </div>
    </div>
  );
}

export default App;
